# ImAudio
ImAudio is a simple open source audio player  
  
using [ImGui](https://github.com/SpaiR/imgui-java) for the User Interface,  
[java-stream-player](https://github.com/goxr3plus/java-stream-player) for audio playing,  
and [java-iso-tools](https://github.com/stephenc/java-iso-tools) for extracting audio from CD's (WIP)

## Name
ImAudio (Im for ImGui and Audio for Audio Player)

## Description
WIP

## Visuals
WIP

## Usage
WIP

## Support
WIP

## Contributing
WIP

## Authors and acknowledgment
PixelDots

## License
GNU General Public License v3.0

